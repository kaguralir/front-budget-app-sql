# THE FRONT OF THE BUDGET APP

CREATED WITH REACT, NODE.JS, SQL

## ABOUT

Allows to enter amount of numbers, positive or negative and get a balance of the total.

### TEST/RUN SITE ON HEROKU

Open [https://budgetappfront-948505.herokuapp.com/](https://budgetappfront-948505.herokuapp.com/) to view it in the browser.

### IMAGES AND EXAMPLES


*![alt text](https://gitlab.com/kaguralir/front-budget-app-sql/-/raw/master/budget1.png)

*![alt text](https://gitlab.com/kaguralir/front-budget-app-sql/-/raw/master/budget2.png)

# THANK YOU !

